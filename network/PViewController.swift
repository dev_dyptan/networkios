protocol PViewController {
    
    func showObjResult(data: Api.ViewData)
    func showJson(str: String)
}
