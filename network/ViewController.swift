import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController, PViewController {
    @IBOutlet var scrollTextView: UITextView!
    @IBOutlet var city: UILabel!
    @IBOutlet var latitude: UILabel!
    @IBOutlet var longitude: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getResult()
    }
    
    func getResult(){
        let api = Api.init(view: self)
        api.loadObj()
        api.loadJSON()
        
    }
    
    func showJson(str: String) {
        scrollTextView.text = str
    }
    
    func showObjResult(data: Api.ViewData){
        city.text = data.cityCode
        latitude.text = data.latitude
        longitude.text = data.longitude
    }
}
