import Alamofire
import AlamofireObjectMapper

class Api {
    let view: PViewController
    
    let URL = "https://api.lufthansa.com/v1/references/airports/TXL?limit=20&offset=0&LHoperated=0"
    let headers: HTTPHeaders = [
        "Accept": "application/json",
        "Authorization": "Bearer gtcxc68kjr5hv46fxgyr2g5g"
    ]
    
    init(view: ViewController) {
        self.view = view
    }
    
    func loadObj() {
        var data = ViewData.init()
        Alamofire.request(URL, headers: headers).validate().responseObject {(response: DataResponse<ResponseModel>) in
            
            switch response.result {
                case .success:
                    let responseModel = response.result.value?.airportResource?.airports?.airport
                    print("Validation Successful")
                    data.cityCode = (responseModel?.cityCode)!
                    data.latitude = String(format: "%.8f", (responseModel?.position?.coordinate?.latitude)!)
                    data.longitude = String(format: "%.8f", (responseModel?.position?.coordinate?.longitude)!)
                self.view.showObjResult(data: data)
                case .failure(let error):
                    print(error)
                    data.error = error as! String
                self.view.showObjResult(data: data)

            }
        }
    }
    
    
    func loadJSON(){
        Alamofire.request(URL, headers: headers).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let json = response.result.value {
//                    self.view.showJson()
                    print("JSON: \(json)")
                }
            
            case .failure(let error):
                print(error)
            }
        }
    }
    
    class ViewData{
        var cityCode: String = ""
        var latitude: String = ""
        var longitude: String = ""
        var error: String = ""
        
        
        
    }

}
