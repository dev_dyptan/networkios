import Foundation
import ObjectMapper

public struct Position: Mappable {

  private struct SerializationKeys {
    static let coordinate = "Coordinate"
  }

  public var coordinate: Coordinate?

  public init?(map: Map){

  }

  public mutating func mapping(map: Map) {
    coordinate <- map[SerializationKeys.coordinate]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = coordinate { dictionary[SerializationKeys.coordinate] = value.dictionaryRepresentation() }
    return dictionary
  }

}
