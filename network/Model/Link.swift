import Foundation
import ObjectMapper

public struct Link: Mappable {

  private struct SerializationKeys {
    static let href = "@Href"
    static let rel = "@Rel"
  }

  public var href: String?
  public var rel: String?

  public init?(map: Map){

  }

  public mutating func mapping(map: Map) {
    href <- map[SerializationKeys.href]
    rel <- map[SerializationKeys.rel]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = href { dictionary[SerializationKeys.href] = value }
    if let value = rel { dictionary[SerializationKeys.rel] = value }
    return dictionary
  }

}
