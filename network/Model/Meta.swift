import Foundation
import ObjectMapper

public struct Meta: Mappable {

  private struct SerializationKeys {
    static let version = "@Version"
    static let link = "Link"
  }

  public var version: String?
  public var link: [Link]?

  public init?(map: Map){

  }

  public mutating func mapping(map: Map) {
    version <- map[SerializationKeys.version]
    link <- map[SerializationKeys.link]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = version { dictionary[SerializationKeys.version] = value }
    if let value = link { dictionary[SerializationKeys.link] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
