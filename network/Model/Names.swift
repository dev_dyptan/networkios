import ObjectMapper

public struct Names: Mappable {

  private struct SerializationKeys {
    static let name = "Name"
  }

  public var name: [Name]?

  public init?(map: Map){

  }

  public mutating func mapping(map: Map) {
    name <- map[SerializationKeys.name]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = name { dictionary[SerializationKeys.name] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
