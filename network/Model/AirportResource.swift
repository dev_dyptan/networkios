import ObjectMapper

public struct AirportResource: Mappable {

  private struct SerializationKeys {
    static let meta = "Meta"
    static let airports = "Airports"
  }

  public var meta: Meta?
  public var airports: Airports?

  public init?(map: Map){

  }

  public mutating func mapping(map: Map) {
    meta <- map[SerializationKeys.meta]
    airports <- map[SerializationKeys.airports]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = meta { dictionary[SerializationKeys.meta] = value.dictionaryRepresentation() }
    if let value = airports { dictionary[SerializationKeys.airports] = value.dictionaryRepresentation() }
    return dictionary
  }

}
