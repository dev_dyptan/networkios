import Foundation
import ObjectMapper

public struct Coordinate: Mappable {

  private struct SerializationKeys {
    static let longitude = "Longitude"
    static let latitude = "Latitude"
  }

  public var longitude: Float?
  public var latitude: Float?

  public init?(map: Map){

  }

  public mutating func mapping(map: Map) {
    longitude <- map[SerializationKeys.longitude]
    latitude <- map[SerializationKeys.latitude]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = longitude { dictionary[SerializationKeys.longitude] = value }
    if let value = latitude { dictionary[SerializationKeys.latitude] = value }
    return dictionary
  }

}
