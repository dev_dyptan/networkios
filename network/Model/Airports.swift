import ObjectMapper

public struct Airports: Mappable {

  private struct SerializationKeys {
    static let airport = "Airport"
  }

  public var airport: Airport?

  public init?(map: Map){

  }

  public mutating func mapping(map: Map) {
    airport <- map[SerializationKeys.airport]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = airport { dictionary[SerializationKeys.airport] = value.dictionaryRepresentation() }
    return dictionary
  }

}
