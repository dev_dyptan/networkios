import ObjectMapper

class ResponseModel: Mappable {
    
  private struct SerializationKeys {
        static let airportResource = "AirportResource"
  }

  public var airportResource: AirportResource?

  public required init?(map: Map){

  }

  public func mapping(map: Map) {
    airportResource <- map[SerializationKeys.airportResource]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = airportResource { dictionary[SerializationKeys.airportResource] = value.dictionaryRepresentation() }
    return dictionary
  }

}
