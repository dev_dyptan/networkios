import ObjectMapper

public struct Airport: Mappable {

  private struct SerializationKeys {
    static let utcOffset = "UtcOffset"
    static let locationType = "LocationType"
    static let timeZoneId = "TimeZoneId"
    static let position = "Position"
    static let airportCode = "AirportCode"
    static let cityCode = "CityCode"
    static let names = "Names"
    static let countryCode = "CountryCode"
  }

  public var utcOffset: Int?
  public var locationType: String?
  public var timeZoneId: String?
  public var position: Position?
  public var airportCode: String?
  public var cityCode: String?
  public var names: Names?
  public var countryCode: String?

  public init?(map: Map){

  }

  public mutating func mapping(map: Map) {
    utcOffset <- map[SerializationKeys.utcOffset]
    locationType <- map[SerializationKeys.locationType]
    timeZoneId <- map[SerializationKeys.timeZoneId]
    position <- map[SerializationKeys.position]
    airportCode <- map[SerializationKeys.airportCode]
    cityCode <- map[SerializationKeys.cityCode]
    names <- map[SerializationKeys.names]
    countryCode <- map[SerializationKeys.countryCode]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = utcOffset { dictionary[SerializationKeys.utcOffset] = value }
    if let value = locationType { dictionary[SerializationKeys.locationType] = value }
    if let value = timeZoneId { dictionary[SerializationKeys.timeZoneId] = value }
    if let value = position { dictionary[SerializationKeys.position] = value.dictionaryRepresentation() }
    if let value = airportCode { dictionary[SerializationKeys.airportCode] = value }
    if let value = cityCode { dictionary[SerializationKeys.cityCode] = value }
    if let value = names { dictionary[SerializationKeys.names] = value.dictionaryRepresentation() }
    if let value = countryCode { dictionary[SerializationKeys.countryCode] = value }
    return dictionary
  }

}
