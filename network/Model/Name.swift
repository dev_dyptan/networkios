import Foundation
import ObjectMapper

public struct Name: Mappable {

  private struct SerializationKeys {
    static let $ = "$"
    static let languageCode = "@LanguageCode"
  }

  public var $: String?
  public var languageCode: String?

  public init?(map: Map){

  }

  public mutating func mapping(map: Map) {
    $ <- map[SerializationKeys.$]
    languageCode <- map[SerializationKeys.languageCode]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = $ { dictionary[SerializationKeys.$] = value }
    if let value = languageCode { dictionary[SerializationKeys.languageCode] = value }
    return dictionary
  }

}
